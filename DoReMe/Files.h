//
//  Files.h
//  DoReMe
//
//  Created by student on 10/23/13.
//  Copyright (c) 2013 stoneenergy.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Files : NSObject

@property (nonatomic) NSArray* files;


@property (nonatomic) NSFileManager* fileManager;
@property (nonatomic) NSString* dirPath;
@property (nonatomic) NSArray* paths;

-(id)initwithUnit: (NSString*)unit andDir: (NSString*)active;
@end
