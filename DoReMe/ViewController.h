//
//  ViewController.h
//  DoReMe
//
//  Created by student on 10/22/13.
//  Copyright (c) 2013 stoneenergy.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *header;
@property (strong, nonatomic) IBOutlet UIButton *teacherhw;
@property (weak, nonatomic) IBOutlet UILabel *teacherhwC;
@property (strong, nonatomic) IBOutlet UIButton *reader;
@property (weak, nonatomic) IBOutlet UILabel *readerC;
@property (strong, nonatomic) IBOutlet UIButton *cd;
@property (weak, nonatomic) IBOutlet UILabel *cdC;
@property (strong, nonatomic) IBOutlet UIButton *movement;
@property (weak, nonatomic) IBOutlet UILabel *movementC;
@property (strong, nonatomic) IBOutlet UIButton *intruments;
@property (weak, nonatomic) IBOutlet UILabel *intrumentsC;

@property(strong,nonatomic)NSString* unit;
@end
