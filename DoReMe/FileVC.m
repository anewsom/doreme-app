//
//  FileVC.m
//  DoReMe
//
//  Created by student on 10/23/13.
//  Copyright (c) 2013 stoneenergy.com. All rights reserved.
//

#import "FileVC.h"
#import "AVFoundation/AVAudioPlayer.h"

@interface FileVC ()
@property (nonatomic) AVAudioPlayer *player;
@end

@implementation FileVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //like load webview
    _webview = [[UIWebView alloc]initWithFrame:self.view.bounds];
    _webview.autoresizingMask=~UIViewAutoresizingNone;
    _webview.scalesPageToFit=YES;
    [self.view addSubview:_webview];
    
    NSString* title =[[_path lastPathComponent]stringByRemovingPercentEncoding];
    self.title=[title substringToIndex:[title rangeOfString:@"."].location];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:_path]];
        [_webview loadRequest:request];
}

-(void)startStop
{
    if(_player.playing)
    {
        [_player stop];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemPlay target:self action:@selector(startStop)];
    }
    else
    {
        [_player play];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemPause target:self action:@selector(startStop)];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
