//
//  Files.m
//  DoReMe
//
//  Created by student on 10/23/13.
//  Copyright (c) 2013 stoneenergy.com. All rights reserved.
//

#import "Files.h"

@implementation Files

-(id)initwithUnit:(NSString*)unit andDir: (NSString*)active
{
    id this=[super init];
    
    //read files from file
    _fileManager=[NSFileManager defaultManager];
    _paths=	NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    _dirPath=[[_paths objectAtIndex:0]stringByAppendingPathComponent:@"Data"];
    
   // NSLog(@"num here : %@",[_fileManager contentsOfDirectoryAtPath:[_dirPath stringByAppendingPathComponent:@"TeacherHomework"] error:nil]);

    _files=[_fileManager contentsOfDirectoryAtPath:[[_dirPath stringByAppendingPathComponent:unit]stringByAppendingPathComponent:active] error:nil];

   
        return this;
}

@end
