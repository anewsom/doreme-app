//
//  ListVC.h
//  DoReMe
//
//  Created by student on 10/23/13.
//  Copyright (c) 2013 stoneenergy.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Files.h"

@interface ListVC : UITableViewController
@property (nonatomic) Files* files;
@property (nonatomic, strong) NSString* active;
@property (nonatomic, strong) NSString* unit;
@end
