//
//  AppDelegate.h
//  DoReMe
//
//  Created by student on 10/22/13.
//  Copyright (c) 2013 stoneenergy.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
