//
//  ListVC.m
//  DoReMe
//
//  Created by student on 10/23/13.
//  Copyright (c) 2013 stoneenergy.com. All rights reserved.
//

#import "ListVC.h"
#import "Files.h"
#import "FileVC.h"

@interface ListVC ()
@property (nonatomic) NSArray* current;
@end

@implementation ListVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //see about image
    UIImageView* iview= [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"banner.png"]];
    [iview setContentMode:UIViewContentModeScaleToFill];
    iview.bounds=CGRectMake(iview.bounds.origin.x, iview.bounds.origin.y, iview.bounds.size.width, 153/2);
    self.tableView.tableHeaderView =iview;
    
    
    _files= [[Files alloc] initwithUnit:_unit andDir: _active];
   // NSLog(@"Loading: %@",_active);

    _current = _files.files;


    self.title = [NSString stringWithFormat:@"Grade: %@, %@",_unit, _active];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemReply target:self action:@selector(goHome)];
}
-(void) goHome
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //NSLog(@"num: %d",_current.count);
    return _current.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    // Configure the cell...
    NSString* title =[_current[indexPath.row]stringByRemovingPercentEncoding];
    cell.textLabel.text=[title substringToIndex:[title rangeOfString:@"."].location];
    //NSLog(@"At: %d %@",indexPath.row,_current[indexPath.row]);
    //cell.detailTextLabel.text=@"optional subtitle";
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //push to 'View' controller
    FileVC *vc = [[FileVC alloc]init];

   NSString* dirPath=[[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]stringByAppendingPathComponent:@"Data"];
    vc.path= [[[dirPath stringByAppendingPathComponent:_unit]stringByAppendingPathComponent:_active]stringByAppendingPathComponent:_current[indexPath.row]];
    NSLog(@"path: %@",vc.path);
    [self.navigationController pushViewController:vc animated:YES];
}



#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
