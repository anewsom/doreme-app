//
//  UnitVC.m
//  DoReMe
//
//  Created by Brian Stanford on 11/8/13.
//  Copyright (c) 2013 stoneenergy.com. All rights reserved.
//

#import "UnitVC.h"
#import "ASIHTTPRequest.h"
#import "ListVC.h"
#import "ViewController.h"

@interface UnitVC ()
@property (nonatomic) NSFileManager* fileManager;
@property (nonatomic) NSString* dirPath;
@property (nonatomic) NSString* dateListPath;//not currently used
@property (nonatomic) NSArray* paths;
@property (nonatomic) NSString* currentFolder;
@property(nonatomic)NSString* currentUnit;
@property(nonatomic)NSString* selectedUnit;
@property(nonatomic)NSMutableArray* units;

@property(nonatomic)NSMutableArray* fileList;//this is used to delete files
@end

@implementation UnitVC

- (void)viewDidLoad
{
    [super viewDidLoad];

    _fileList= [[NSMutableArray alloc]init];
    
    UIImageView* iview= [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"banner.png"]];
    [iview setContentMode:UIViewContentModeScaleToFill];
    iview.bounds=CGRectMake(iview.bounds.origin.x, iview.bounds.origin.y, iview.bounds.size.width, 153/2);
    self.tableView.tableHeaderView =iview;
    
    //load file mgr
    _fileManager=[NSFileManager defaultManager];
    _paths=	NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    _dirPath=[[_paths objectAtIndex:0]stringByAppendingPathComponent:@"Data"];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh:)];
    
    [self loadFileData];
}

-(void) loadFileData
{
    _units=[[_fileManager contentsOfDirectoryAtPath:_dirPath error:nil]mutableCopy];
    [self.tableView reloadData];
}
#pragma mark - Navigation

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    ViewController* vc = [sb instantiateViewControllerWithIdentifier:@"VC"];
     vc.unit=_units[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _units.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    cell.textLabel.text=_units[indexPath.row];
    return cell;
}
-(IBAction)refresh:(id)sender
{
    NSLog(@"Refresh");
    //do web query, compare it to the files on disk, and downlaod missing files
    //get xml file
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    [self performSelectorInBackground:@selector(backgroundRefresh) withObject:nil];
}

-(void) backgroundRefresh
{
    NSXMLParser* xmlParser=[[NSXMLParser alloc] initWithContentsOfURL:[NSURL URLWithString:@"http://www.stoneenergy.com/doreme/DoReME/index.xml"]];
    [xmlParser setDelegate:self];
    
    bool success = [xmlParser parse];
    NSString* suc; //this is so we can pass a id and not a bool
    if (success)
        suc=@"YES";
    else
        suc=@"NO";
    
    // look through cache dir and delete any files that are not in filelist (3 levels)
    bool emptyUnit;
    for (NSString* unit in [_fileManager contentsOfDirectoryAtPath:_dirPath error:nil]) {
        emptyUnit = true;
        for (NSString*sub in [_fileManager contentsOfDirectoryAtPath:[_dirPath stringByAppendingPathComponent:unit] error:nil]) {
            for (NSString*file in [_fileManager contentsOfDirectoryAtPath:[[_dirPath stringByAppendingPathComponent:unit]stringByAppendingPathComponent:sub] error:nil]) {
                NSString* fileP=[[[_dirPath stringByAppendingPathComponent:unit]stringByAppendingPathComponent:sub] stringByAppendingPathComponent:file];
                NSLog(@"File in P: %@",fileP);
                emptyUnit=false;
                if (![_fileList containsObject:fileP]) {
                    bool su=[_fileManager removeItemAtPath:fileP error:nil];
                    NSLog(@"removed s=%d: %@",su,fileP);
                }
            }
        }
        //remove empty units
        if(emptyUnit)
        {
            NSString* unitP=[_dirPath stringByAppendingPathComponent:unit];
            bool su=[_fileManager removeItemAtPath:unitP error:nil];
            NSLog(@"removed unit s=%d: %@",su,unitP);
        }
    }
    
    [_fileList removeAllObjects];
    [self performSelectorOnMainThread:@selector(finishRefresh:) withObject:suc waitUntilDone:NO];
}

-(void) finishRefresh:(NSString*)success
{
    if([success isEqualToString:@"NO"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet Detected!"
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK", nil];
        [alert show];
    }
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    [self loadFileData];
}

#pragma mark - NSXMLParser delgates
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    @try {

    //NSLog(@"Start:%@ withDic:%@",elementName,attributeDict.debugDescription);
    if ([elementName isEqualToString:@"Unit"])
    {
        _currentUnit = [attributeDict valueForKey:@"Name"];
        NSString* unitPath=[_dirPath stringByAppendingPathComponent:_currentUnit];
        if (![_fileManager fileExistsAtPath:unitPath]) {
            [_fileManager createDirectoryAtPath:[unitPath stringByAppendingPathComponent:@"TeacherHomework"] withIntermediateDirectories:YES attributes:nil error:nil];
            [_fileManager createDirectoryAtPath:[unitPath stringByAppendingPathComponent:@"Reading"] withIntermediateDirectories:YES attributes:nil error:nil];
            [_fileManager createDirectoryAtPath:[unitPath stringByAppendingPathComponent:@"CD_Listening"] withIntermediateDirectories:YES attributes:nil error:nil];
            [_fileManager createDirectoryAtPath:[unitPath stringByAppendingPathComponent:@"Movement"] withIntermediateDirectories:YES attributes:nil error:nil];
            [_fileManager createDirectoryAtPath:[unitPath stringByAppendingPathComponent:@"Instruments"] withIntermediateDirectories:YES attributes:nil error:nil];
        }
    }
    if ([elementName isEqualToString:@"Subject"])
        _currentFolder = [attributeDict valueForKey:@"Name"];
    else if ([elementName isEqualToString:@"Lesson"])
    {
        NSString* path = [[attributeDict valueForKey:@"path"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //see if file exists
        NSString* fullPath = [[[_dirPath stringByAppendingPathComponent:_currentUnit]stringByAppendingPathComponent:_currentFolder]stringByAppendingPathComponent:[path substringFromIndex:[path rangeOfString:@"/" options:NSBackwardsSearch].location+1]];
        NSLog(@"Added: %@",fullPath);
        
        [_fileList addObject:fullPath];//add to filelist for deleting missing files
        
        if (![_fileManager fileExistsAtPath:fullPath]) {
            //download file
            ASIHTTPRequest *downloadRequest = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:path]];
            [downloadRequest setDownloadDestinationPath:fullPath];
            NSLog(@"Starting download at %@",fullPath);
            [downloadRequest startSynchronous];
        }
        else
            NSLog(@"no download required");
    }
    else
        NSLog(@"Unknown XML element");
        
    }
    @catch (NSException *exception) {
      
        NSLog(@"There is an error: %@",exception.description);
        [self performSelectorOnMainThread:@selector(error:) withObject:exception.description waitUntilDone:YES];
        
    }
    @finally {
        
    }
    
}
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
}
-(void)error:(NSString *) err
{
    [[[UIAlertView alloc]initWithTitle:@"Error" message:err delegate:self cancelButtonTitle:@"Exit" otherButtonTitles:nil]show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    exit(1);
}

@end
