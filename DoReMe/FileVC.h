//
//  FileVC.h
//  DoReMe
//
//  Created by student on 10/23/13.
//  Copyright (c) 2013 stoneenergy.com. All rights reserved.
//

#import "ViewController.h"
#import "AVFoundation/AVAudioPlayer.h"

@interface FileVC : UIViewController <AVAudioPlayerDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *webview;
@property(strong,nonatomic) NSString* path;

@end
