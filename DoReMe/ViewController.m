//
//  ViewController.m
//  DoReMe
//
//  Created by student on 10/22/13.
//  Copyright (c) 2013 stoneenergy.com. All rights reserved.
//

#import "ViewController.h"
#import "ListVC.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    _header.image=[UIImage imageNamed:@"doremetitle.png"];
    [_teacherhw setBackgroundImage:[UIImage imageNamed:@"doremeteacherhw.png"]forState:UIControlStateNormal];
    [_reader setBackgroundImage:[UIImage imageNamed:@"doremereading.png"]forState:UIControlStateNormal];
    [_cd setBackgroundImage:[UIImage imageNamed:@"doremecd.png"]forState:UIControlStateNormal];
    [_movement setBackgroundImage:[UIImage imageNamed:@"dorememovement.png"] forState:UIControlStateNormal];
    
    [_intruments setBackgroundImage:[UIImage imageNamed:@"doremeintruments.png"] forState:UIControlStateNormal];
    [_teacherhw setBackgroundImage:[UIImage imageNamed:@"doremeteacherhw.png"] forState:UIControlStateNormal];
    
    self.title=_unit;
    
    //check list and add numbers
    NSFileManager *_fileManager=[NSFileManager defaultManager];
    NSString *_dirPath=[[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]stringByAppendingPathComponent:@"Data"];
    
    // NSLog(@"num here : %@",[_fileManager contentsOfDirectoryAtPath:[_dirPath stringByAppendingPathComponent:@"TeacherHomework"] error:nil]);
    
   _teacherhwC.text =[NSString stringWithFormat:@"%lu", (unsigned long)[_fileManager contentsOfDirectoryAtPath:[[_dirPath stringByAppendingPathComponent:_unit]stringByAppendingPathComponent:_teacherhw.titleLabel.text] error:nil].count ];
    
       _readerC.text =[NSString stringWithFormat:@"%lu", (unsigned long)[_fileManager contentsOfDirectoryAtPath:[[_dirPath stringByAppendingPathComponent:_unit]stringByAppendingPathComponent:_reader.titleLabel.text] error:nil].count ];
    
       _cdC.text =[NSString stringWithFormat:@"%lu", (unsigned long)[_fileManager contentsOfDirectoryAtPath:[[_dirPath stringByAppendingPathComponent:_unit]stringByAppendingPathComponent:_cd.titleLabel.text] error:nil].count ];
    
       _movementC.text =[NSString stringWithFormat:@"%lu", (unsigned long)[_fileManager contentsOfDirectoryAtPath:[[_dirPath stringByAppendingPathComponent:_unit]stringByAppendingPathComponent:_movement.titleLabel.text] error:nil].count ];
    
       _intrumentsC.text =[NSString stringWithFormat:@"%lu", (unsigned long)[_fileManager contentsOfDirectoryAtPath:[[_dirPath stringByAppendingPathComponent:_unit]stringByAppendingPathComponent:_intruments.titleLabel.text] error:nil].count ];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ListVC*vc =[segue destinationViewController];
    vc.active=((UIButton*)sender).titleLabel.text;
    vc.unit=_unit;
    NSLog(@"set: %@",vc.active);
}



@end
